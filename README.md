# JQuery Cat Slide

* @Project Name   - jqueryCatSlide
* @Script Version - 1.0
* @Author Name    - Amyth Arora (aka Amit Arora)
* @Author Email   - codigosenor@gmail.com



###DESCRIPTION :

jqueryCatSlide is a Jquery based Slide show that can handle Multiple Categories. You can have any number of categories of in your slide show. I have tried to make this as reusable as i could and i hope this project helps you.

###FEATURES :

1.Support Multiple Categories
2.Support Thumbnail Navigation
3.AutoPlay Feature
4.Custom Height / Width


###SETUP / USAGE :

1.  Include the latest version of Jquery Library between the <head> </head> tags of your HTML file.

2.  Include "jquerycatslide.js" or "jquerycatslide.min.js" between the <head> </head> section of your HTML file.

3.  Add Images in the Following Format :

      **Basic SlideShow
      `<div id="your-slide-show-div-id-here">
        <img src="your-image-1.jpg" class="slide" />
        <img src="your-image-2.jpg" class="slide" />
        <img src="your-image-3.jpg" class="slide" />
      </div>`

      ** Category Based SlideShow

      `<div id="your-slide-show-div-id-here">
        <div name="first-category-name-here">
          <img src="your-image-1.jpg" class="slide" />
          <img src="your-image-2.jpg" class="slide" />
          <img src="your-image-3.jpg" class="slide" />
        </div>
        <div name="second-category-name-here">
          <img src="your-image-4.jpg" class="slide" />
          <img src="your-image-5.jpg" class="slide" />
          <img src="your-image-6.jpg" class="slide" />
        </div>
      </div>`

4.  To Initiate the SlideShow include the following script between the <head></head> section:

      `<script type="text/javascript">
        $.(document).ready( function() {
          $("#your-slide-show-div-id-here").catslide();
        });
      </script>`

5.  To Setup Options use the script as follows:

      `<script type="text/javascript">
        $.(document).ready( function() {
          $("#your-slide-show-div-id-here").catslide({
            autoPlay: true,
            effect: 'fade'
          });
        });
      </script>`


###TO BE INTRODUCED IN NEXT VERSION:

* Multiple Effects
* Video / Other Content can be Included in Slides
* Hyperlinks
* FullScreen Gallery Option
