/*----------------------------------------*
 * @Title : Jquery Cat Slide
  * @Author : Amyth Arora (aka Amit Arora)
  * @Author Email: codigosenor@gmail.com
*----------------------------------------*/

(function($) {
  $.fn.catslide = function(options){

    // Merge/Override User Defined Options
    options = $.extend({}, $.fn.catslide.option, options);

    var entity = this;
    var activeImage = 0;
    var totalImages = 0;
    var totalCatImages = 0;
    var categoryList = {};
    var imageArray = [];
    var activeCatName = '';
    var activeImageList = [];
    var slideShow;

    //Function to Create List of Images
    function createImageArray(){
      entity.find("img").each( function () {
        imageArray.push(this.src);
      });
    }

    //Function to Create a List of Categories
    function createCategories(){
      entity.find("div").each( function() {
        var categoryname = ($(this).attr('name').replace(' ', '_'));
        var imageList = []
        $(this).find("img").each( function() {
          imageList.push(this);
        });
        categoryList[categoryname] = imageList;
        });
    }

    //Function to Change Category
    function changeCategory(category){
      clearInterval(slideShow);
      $(activeImageList).eq(activeImage).fadeOut(options.interval);
      // Reset the Active Class
      entity.find("div.active").removeClass('active');
      $('div[name=' + category + ']').addClass('active');
      // Set the Active Category Name
      activeCatName = category;
      if (options.showThumbnailNavigation === true){
        // First Remove Old Thumbnails
        $('#thumbnail-navigation').remove();
        $('#thumbnailsLeft').remove();
        $('#thumbnailsRight').remove();
      }
      activeImage = 0;
      slideShowReady();
      playSlideShow(activeImageList);
    }

    // Function to Make the SlideShow ready for action
    function slideShowReady(){
      // Hide all the Images in All the Categories
      $.each(categoryList, function(k, v){
        $.each(v, function(){
          $(this).css({'display': 'none'});
        });
      });
      // Set the Active Category Name
      activeCatName = entity.find("div.active").attr('name').replace(' ', '_');
      // Set the Active Image List
      activeImageList = categoryList[activeCatName];
      // Set the Total Number of Slides in Active Image List
      totalImages = $(activeImageList).length;
      // Create Thumbnails if Thumbnail Navigation is On.
      if (options.showThumbnailNavigation === true){
        $('#jcs-container').append('<div id="thumbnail-navigation" style="width: '+ totalImages * 60 +'px;"></div>');
        $('#jcs-container').css({'height' : options.catslideHeight + 60});
        generateThumbnails();
        if ($('#jcs-container').width() < $('#thumbnail-navigation').width()){
          $('#jcs-container').append(
            '<a id="thumbnailsLeft" class="thumbnail-controller"><</a>'+
            '<a id="thumbnailsRight" class="thumbnail-controller">></a>'
          );
        }
      }
      // Show the first SLide
      $(activeImageList).eq(activeImage).fadeIn(options.interval);
      if (options.imageDescription === true){
        showImageDescription(activeImage);
      }
    }

    function playSlideShow(imageList) {
      slideShow = setInterval(function play(){
        // Run the SlideShow Loop
        if (options.catChanged === true){
          return false;
        }
        if (options.isPaused === false){
          $(imageList).eq(activeImage).fadeOut(options.interval);
          if (activeImage >= totalImages - 1){
            activeImage = 0;
          } else {
            activeImage++;
          }
        } else {
          // Reset Slide Show
        }
        $(imageList).eq(activeImage).fadeIn(options.interval);
        showImageDescription(activeImage);
      }, options.delay);

      // Thumbnail Click Function
      $('.jcs-thumb').click( function(){
        loadSpecificImage($(this).attr('id').split('thumb_')[1]);
      });
      // Thumbnail Scroll Contoller
      $('#thumbnail-navigation').hover( function(){
        $('.thumbnail-controller').fadeIn(1000);
      });
      $('#thumbnailsLeft').hover( function(){
        scrollThumbnailsLeft();
      });
      $('#thumbnailsRight').hover( function(){
        scrollThumbnailsRight();
      });
    }

    function loadSpecificImage(imageid) {
      if (!( activeImage === imageid)){
        $(activeImageList).eq(activeImage).fadeOut(options.interval);
        activeImage = imageid;
        $(activeImageList).eq(activeImage).fadeIn(options.interval);
        showImageDescription(activeImage);
      } else {

      }
      options.isPaused = true;
      var wait = setTimeout (function () {
        resetSlideShow();
      }, options.delay);
    }

    function showImageDescription(image){
      $('.jcs-img-desc').fadeOut(300);
      $('.jcs-img-desc').remove();
      var desc = ($(activeImageList).eq(image).attr('alt'));
      var descHeading = desc.split(' : ')[0];
      var descContent = desc.split(' : ')[1];
      if(!(desc === '')){
        // Create Decription Div's on the Fly and Slide them In
        $('#jcs-container').append('<div class="jcs-img-desc"><div class="jcs-desc-heading">' + descHeading + '</div><div class="jcs-desc-content">' + descContent + '</div></div>');
        $('.jcs-img-desc').fadeIn(options.interval);
      }
    }

    function resetSlideShow() {
      options.isPaused = false;
    }

    function generateThumbnails(){
      $.each(activeImageList, function(){
        $('div#thumbnail-navigation').append('<div id="' +
          activeCatName + '_thumb_' + $(this).index() +
          '" class="jcs-thumb"><img src="' +
          $(this).attr('src') + '" /></div>');
      });
    }

    function scrollThumbnailsLeft(){
      var leftPos = $('#thumbnail-navigation').position().left;
      if (leftPos >= -60){
        $('#thumbnail-navigation').animate({left: '0'});
      }
      if (!(leftPos >= -60)){
        $('#thumbnail-navigation').animate({left: '+=60'});
      }
    }

    function scrollThumbnailsRight(){
      var ssWidth = $('#jcs-container').width();
      var tcWidth = $('#thumbnail-navigation').width();
      var leftPos = $('#thumbnail-navigation').position().left;
      if (leftPos <= ssWidth - tcWidth){
        $('#thumbnail-navigation').animate({left: ssWidth - tcWidth});
      }
      if (!(leftPos <= ssWidth - tcWidth)){
        $('#thumbnail-navigation').animate({left: '-=60'});
      }
    }

    //Wrap The Slide Show Div with the Cat Control Panel and set Height and Width
    this.wrap('<div id="jcs-container"><div id="the-cat-slideshow"> </div></div>');
    $('#jcs-container').css({'height' : options.catslideHeight, 'width' : options.catslideWidth})
    $('#the-cat-slideshow').css({'height': options.catslideHeight, 'width': options.catslideWidth});
    this.find("img").css({'max-width': '100%', 'position': 'absolute', 'top': '0px', 'left': '170px'});
    $('#jcs-container').parent().append('<div style="clear:both;"></div>');
    if (options.showCategories === true ) {
      createCategories();
      $('#the-cat-slideshow').append('<ul id="category-control-panel"></ul>');
      $.each(categoryList, function(k, v){
        $('#category-control-panel').append('<li class="jcs-category" id="category__' + k.replace(' ', '') + '">' + k.replace('_', ' ') + '</li>');
      });
    }
    // get the Slide Show Elements in Position and Get them Ready to Kick Ass
    slideShowReady();
    // Action !!
    if (options.autoPlay === true){
      playSlideShow(activeImageList);
    }

    //Category Click Function
    $('.jcs-category').click ( function() {
      changeCategory($(this).attr('id').split('category__')[1]);
    });
  }


  // Function to Preload Images
  function preloadImages(imagearray){
    $(imagearray).each( function() {
      $('<img/>')[0].src = this;
    });
  }

  // Default Cat Slide Options
  $.fn.catslide.option = {
    autoPlay: true, // option to switch on/off autoplay
    categorized: true, // option to choose between categorized or uncategorized slideshow.
    showCategories: true, // option to switch on category list in the slideshow.
    showThumbnailNavigation: true, // option to choose if you wish to show thumbnails or not.
    effect: 'slide', // option to choose from multiple slide show animations ('slide', 'fade', 'curtainx', 'curtainy' )
    catslideHeight: 460, // Slide Show Height in Pixels
    catslideWidth: 1000, // Slide Show Width in Pixels
    interval: 1000, //Slide Change interval in milliseconds
    delay: 6000, // Slide Change Delay
    isPaused: false, // Slide Show Pause State
    catChanged: false, // Category Change State
    imageDescription: true // Show Description Switch for Images "Alt" tag
  }
})(jQuery);

